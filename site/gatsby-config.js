module.exports = {
  siteMetadata: {
    siteTitle: 'Media Death Match',
    siteDescription: 'Grab your popcorn!',
    siteImage: '/banner.png', // main image of the site for metadata
    siteUrl: 'https://mediadeathmatch.com/',
    pathPrefix: '/',
    siteLanguage: 'en',
    ogLanguage: `en_US`,
    author: 'A. Nonee Muss', // for example - 'Ivan Ganev'
    authorDescription: 'short author description', // short text about the author
    avatar: '/avatar.jpg',
    twitterSite: '', // website account on twitter
    twitterCreator: '', // creator account on twitter
    social: [
      {
        icon: `at`,
        url: `mailto:mymail@mail.com`
      },
      {
        icon: `twitter`,
        url: `https://twitter.com/mediadeathmatch`
      },
      {
        icon: `github`,
        url: `https://gitlab.com/mediadeathmatch/gatsby-theme-mdm`
      },
      {
        icon: `node-js`,
        url: `https://www.npmjs.com/package/gatsby-theme-mdm`
      }
    ]
  },
  plugins: [
    {
      resolve: "@mediadeathmatch/gatsby-theme-mdm",
      options: {
        contentPath: "events",
        basePath: "/events",
      },
    },
    {
      resolve: 'gatsby-theme-chronoblog',
      options: {
        uiText: {
          // ui text fot translate
          feedShowMoreButton: 'show more',
          feedSearchPlaceholder: 'search',
          cardReadMoreButton: 'read more →',
          allTagsButton: 'all tags'
        },
        feedItems: {
          // global settings for feed items
          limit: 50,
          yearSeparator: true,
          yearSeparatorSkipFirst: true,
          contentTypes: {
            links: {
              beforeTitle: '🔗 '
            }
          }
        },
        feedSearch: {
          symbol: '🔍'
        }
      }
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Chronoblog Gatsby Theme`,
        short_name: `Chronoblog`,
        start_url: `/`,
        background_color: `#fff`,
        theme_color: `#3a5f7d`,
        display: `standalone`,
        icon: `src/assets/favicon.png`
      }
    },
    {
      resolve: `gatsby-plugin-sitemap`
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        // replace "UA-XXXXXXXXX-X" with your own Tracking ID
        trackingId: 'UA-XXXXXXXXX-X'
      }
    }
  ]
};
