---
date: 2020-07-25
tags: ['versus']
---

<div>
  <div style={{ width: `50%`, position: `relative`, float: `left`}} >
    <Embed
      src="https://www.youtube.com/embed/lmAJGymXXAc"
    />
    Coffee
  </div>
  <div style={{ width: `50%`, position: `relative`, float: `left`}} >
    <Embed
      src="https://www.youtube.com/embed/FLCaqhZ67zI"
    />
    Wine
  </div>
</div>
