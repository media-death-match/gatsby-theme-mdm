# @mediadeathmatch/gatsby-theme-mdm

This is a theme built using this
[guide](https://www.gatsbyjs.org/tutorial/building-a-theme/).

Halted about midway through the tutorial
[here](https://www.gatsbyjs.org/tutorial/building-a-theme/#create-the-events-and-event-template-components).

### Makefile

`make s` -- will get you a `gatsby development` running in the site
directory.

`make serval` -- Just like `make s` except it will listen on `0.0.0.0`
instead of just localhost.
